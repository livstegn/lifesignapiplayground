/*
 Copyright 2010-2018 Amazon.com, Inc. or its affiliates. All Rights Reserved.

 Licensed under the Apache License, Version 2.0 (the "License").
 You may not use this file except in compliance with the License.
 A copy of the License is located at

 http://aws.amazon.com/apache2.0

 or in the "license" file accompanying this file. This file is distributed
 on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 express or implied. See the License for the specific language governing
 permissions and limitations under the License.
 */


import AWSCore
import AWSAPIGateway

public class AWSLIFESIGNSDKLifesignClient: AWSAPIGatewayClient {

	static let AWSInfoClientKey = "AWSLIFESIGNSDKLifesignClient"

	private static let _serviceClients = AWSSynchronizedMutableDictionary()
	private static let _defaultClient:AWSLIFESIGNSDKLifesignClient = {
		var serviceConfiguration: AWSServiceConfiguration? = nil
        let serviceInfo = AWSInfo.default().defaultServiceInfo(AWSInfoClientKey)
        if let serviceInfo = serviceInfo {
            serviceConfiguration = AWSServiceConfiguration(region: serviceInfo.region, credentialsProvider: serviceInfo.cognitoCredentialsProvider)
        } else if (AWSServiceManager.default().defaultServiceConfiguration != nil) {
            serviceConfiguration = AWSServiceManager.default().defaultServiceConfiguration
        } else {
            serviceConfiguration = AWSServiceConfiguration(region: .Unknown, credentialsProvider: nil)
        }
        
        return AWSLIFESIGNSDKLifesignClient(configuration: serviceConfiguration!)
	}()
    
	/**
	 Returns the singleton service client. If the singleton object does not exist, the SDK instantiates the default service client with `defaultServiceConfiguration` from `AWSServiceManager.defaultServiceManager()`. The reference to this object is maintained by the SDK, and you do not need to retain it manually.
	
	 If you want to enable AWS Signature, set the default service configuration in `func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?)`
	
	     func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
	        let credentialProvider = AWSCognitoCredentialsProvider(regionType: .USEast1, identityPoolId: "YourIdentityPoolId")
	        let configuration = AWSServiceConfiguration(region: .USEast1, credentialsProvider: credentialProvider)
	        AWSServiceManager.default().defaultServiceConfiguration = configuration
	 
	        return true
	     }
	
	 Then call the following to get the default service client:
	
	     let serviceClient = AWSLIFESIGNSDKLifesignClient.default()

     Alternatively, this configuration could also be set in the `info.plist` file of your app under `AWS` dictionary with a configuration dictionary by name `AWSLIFESIGNSDKLifesignClient`.
	
	 @return The default service client.
	 */ 
	 
	public class func `default`() -> AWSLIFESIGNSDKLifesignClient{
		return _defaultClient
	}

	/**
	 Creates a service client with the given service configuration and registers it for the key.
	
	 If you want to enable AWS Signature, set the default service configuration in `func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?)`
	
	     func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
	         let credentialProvider = AWSCognitoCredentialsProvider(regionType: .USEast1, identityPoolId: "YourIdentityPoolId")
	         let configuration = AWSServiceConfiguration(region: .USWest2, credentialsProvider: credentialProvider)
	         AWSLIFESIGNSDKLifesignClient.registerClient(withConfiguration: configuration, forKey: "USWest2AWSLIFESIGNSDKLifesignClient")
	
	         return true
	     }
	
	 Then call the following to get the service client:
	
	
	     let serviceClient = AWSLIFESIGNSDKLifesignClient.client(forKey: "USWest2AWSLIFESIGNSDKLifesignClient")
	
	 @warning After calling this method, do not modify the configuration object. It may cause unspecified behaviors.
	
	 @param configuration A service configuration object.
	 @param key           A string to identify the service client.
	 */
	
	public class func registerClient(withConfiguration configuration: AWSServiceConfiguration, forKey key: String){
		_serviceClients.setObject(AWSLIFESIGNSDKLifesignClient(configuration: configuration), forKey: key  as NSString);
	}

	/**
	 Retrieves the service client associated with the key. You need to call `registerClient(withConfiguration:configuration, forKey:)` before invoking this method or alternatively, set the configuration in your application's `info.plist` file. If `registerClientWithConfiguration(configuration, forKey:)` has not been called in advance or if a configuration is not present in the `info.plist` file of the app, this method returns `nil`.
	
	 For example, set the default service configuration in `func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) `
	
	     func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
	         let credentialProvider = AWSCognitoCredentialsProvider(regionType: .USEast1, identityPoolId: "YourIdentityPoolId")
	         let configuration = AWSServiceConfiguration(region: .USWest2, credentialsProvider: credentialProvider)
	         AWSLIFESIGNSDKLifesignClient.registerClient(withConfiguration: configuration, forKey: "USWest2AWSLIFESIGNSDKLifesignClient")
	
	         return true
	     }
	
	 Then call the following to get the service client:
	 
	 	let serviceClient = AWSLIFESIGNSDKLifesignClient.client(forKey: "USWest2AWSLIFESIGNSDKLifesignClient")
	 
	 @param key A string to identify the service client.
	 @return An instance of the service client.
	 */
	public class func client(forKey key: String) -> AWSLIFESIGNSDKLifesignClient {
		objc_sync_enter(self)
		if let client: AWSLIFESIGNSDKLifesignClient = _serviceClients.object(forKey: key) as? AWSLIFESIGNSDKLifesignClient {
			objc_sync_exit(self)
		    return client
		}

		let serviceInfo = AWSInfo.default().defaultServiceInfo(AWSInfoClientKey)
		if let serviceInfo = serviceInfo {
			let serviceConfiguration = AWSServiceConfiguration(region: serviceInfo.region, credentialsProvider: serviceInfo.cognitoCredentialsProvider)
			AWSLIFESIGNSDKLifesignClient.registerClient(withConfiguration: serviceConfiguration!, forKey: key)
		}
		objc_sync_exit(self)
		return _serviceClients.object(forKey: key) as! AWSLIFESIGNSDKLifesignClient;
	}

	/**
	 Removes the service client associated with the key and release it.
	 
	 @warning Before calling this method, make sure no method is running on this client.
	 
	 @param key A string to identify the service client.
	 */
	public class func removeClient(forKey key: String) -> Void{
		_serviceClients.remove(key)
	}
	
	init(configuration: AWSServiceConfiguration) {
	    super.init()
	
	    self.configuration = configuration.copy() as! AWSServiceConfiguration
	    var URLString: String = "https://y15h3ayew7.execute-api.eu-central-1.amazonaws.com/test"
	    if URLString.hasSuffix("/") {
	        URLString = URLString.substring(to: URLString.index(before: URLString.endIndex))
	    }
	    self.configuration.endpoint = AWSEndpoint(region: configuration.regionType, service: .APIGateway, url: URL(string: URLString))
	    let signer: AWSSignatureV4Signer = AWSSignatureV4Signer(credentialsProvider: configuration.credentialsProvider, endpoint: self.configuration.endpoint)
	    if let endpoint = self.configuration.endpoint {
	    	self.configuration.baseURL = endpoint.url
	    }
	    self.configuration.requestInterceptors = [AWSNetworkingRequestInterceptor(), signer]
	}

	
    /*
     
     
     
     return type: Empty
     */
    public func usersGet() -> AWSTask<Empty> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    let queryParameters:[String:Any] = [:]
	    
	    let pathParameters:[String:Any] = [:]
	    
	    return self.invokeHTTPRequest("GET", urlString: "/users", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: Empty.self) as! AWSTask<Empty>
	}

	
    /*
     
     
     @param email 
     @param name 
     @param cellno 
     @param deviceid 
     @param platform 
     
     return type: Empty
     */
    public func usersPost(email: String, name: String, cellno: String?, deviceid: String?, platform: String?) -> AWSTask<Empty> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    var queryParameters:[String:Any] = [:]
	    queryParameters["cellno"] = cellno
	    queryParameters["email"] = email
	    queryParameters["deviceid"] = deviceid
	    queryParameters["platform"] = platform
	    queryParameters["name"] = name
	    
	    let pathParameters:[String:Any] = [:]
	    
	    return self.invokeHTTPRequest("POST", urlString: "/users", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: Empty.self) as! AWSTask<Empty>
	}

	
    /*
     
     
     @param email 
     
     return type: Empty
     */
    public func usersSearchGet(email: String) -> AWSTask<Empty> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    var queryParameters:[String:Any] = [:]
	    queryParameters["email"] = email
	    
	    let pathParameters:[String:Any] = [:]
	    
	    return self.invokeHTTPRequest("GET", urlString: "/users/search", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: Empty.self) as! AWSTask<Empty>
	}

	
    /*
     
     
     @param userid 
     
     return type: Empty
     */
    public func usersUseridGet(userid: String) -> AWSTask<Empty> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    let queryParameters:[String:Any] = [:]
	    
	    var pathParameters:[String:Any] = [:]
	    pathParameters["userid"] = userid
	    
	    return self.invokeHTTPRequest("GET", urlString: "/users/{userid}", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: Empty.self) as! AWSTask<Empty>
	}

	
    /*
     
     
     @param userid 
     @param cellno 
     @param email 
     @param deviceid 
     @param platform 
     @param name 
     
     return type: Empty
     */
    public func usersUseridPut(userid: String, cellno: String?, email: String?, deviceid: String?, platform: String?, name: String?) -> AWSTask<Empty> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    var queryParameters:[String:Any] = [:]
	    queryParameters["cellno"] = cellno
	    queryParameters["email"] = email
	    queryParameters["deviceid"] = deviceid
	    queryParameters["platform"] = platform
	    queryParameters["name"] = name
	    
	    var pathParameters:[String:Any] = [:]
	    pathParameters["userid"] = userid
	    
	    return self.invokeHTTPRequest("PUT", urlString: "/users/{userid}", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: Empty.self) as! AWSTask<Empty>
	}

	
    /*
     
     
     @param userid 
     
     return type: Empty
     */
    public func usersUseridCaregroupsGet(userid: String) -> AWSTask<Empty> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    let queryParameters:[String:Any] = [:]
	    
	    var pathParameters:[String:Any] = [:]
	    pathParameters["userid"] = userid
	    
	    return self.invokeHTTPRequest("GET", urlString: "/users/{userid}/caregroups", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: Empty.self) as! AWSTask<Empty>
	}

	
    /*
     
     
     @param userid 
     @param caregroupname 
     @param status 
     
     return type: Empty
     */
    public func usersUseridCaregroupsPost(userid: String, caregroupname: String, status: String?) -> AWSTask<Empty> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    var queryParameters:[String:Any] = [:]
	    queryParameters["caregroupname"] = caregroupname
	    queryParameters["status"] = status
	    
	    var pathParameters:[String:Any] = [:]
	    pathParameters["userid"] = userid
	    
	    return self.invokeHTTPRequest("POST", urlString: "/users/{userid}/caregroups", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: Empty.self) as! AWSTask<Empty>
	}

	
    /*
     
     
     @param userid 
     @param caregroupid 
     
     return type: Empty
     */
    public func usersUseridCaregroupsCaregroupidGet(userid: String, caregroupid: String) -> AWSTask<Empty> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    let queryParameters:[String:Any] = [:]
	    
	    var pathParameters:[String:Any] = [:]
	    pathParameters["userid"] = userid
	    pathParameters["caregroupid"] = caregroupid
	    
	    return self.invokeHTTPRequest("GET", urlString: "/users/{userid}/caregroups/{caregroupid}", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: Empty.self) as! AWSTask<Empty>
	}

	
    /*
     
     
     @param userid 
     @param caregroupid 
     
     return type: Empty
     */
    public func usersUseridCaregroupsCaregroupidMembersGet(userid: String, caregroupid: String) -> AWSTask<Empty> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    let queryParameters:[String:Any] = [:]
	    
	    var pathParameters:[String:Any] = [:]
	    pathParameters["userid"] = userid
	    pathParameters["caregroupid"] = caregroupid
	    
	    return self.invokeHTTPRequest("GET", urlString: "/users/{userid}/caregroups/{caregroupid}/members", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: Empty.self) as! AWSTask<Empty>
	}

	
    /*
     
     
     @param userid 
     @param caregroupid 
     @param status 
     
     return type: Empty
     */
    public func usersUseridCaregroupsCaregroupidMembersPost(userid: String, caregroupid: String, status: String?) -> AWSTask<Empty> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    var queryParameters:[String:Any] = [:]
	    queryParameters["status"] = status
	    
	    var pathParameters:[String:Any] = [:]
	    pathParameters["userid"] = userid
	    pathParameters["caregroupid"] = caregroupid
	    
	    return self.invokeHTTPRequest("POST", urlString: "/users/{userid}/caregroups/{caregroupid}/members", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: Empty.self) as! AWSTask<Empty>
	}

	
    /*
     
     
     @param memberid 
     @param userid 
     @param caregroupid 
     
     return type: Empty
     */
    public func usersUseridCaregroupsCaregroupidMembersMemberidRolesGet(memberid: String, userid: String, caregroupid: String) -> AWSTask<Empty> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    let queryParameters:[String:Any] = [:]
	    
	    var pathParameters:[String:Any] = [:]
	    pathParameters["memberid"] = memberid
	    pathParameters["userid"] = userid
	    pathParameters["caregroupid"] = caregroupid
	    
	    return self.invokeHTTPRequest("GET", urlString: "/users/{userid}/caregroups/{caregroupid}/members/{memberid}/roles", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: Empty.self) as! AWSTask<Empty>
	}

	
    /*
     
     
     @param memberid 
     @param userid 
     @param caregroupid 
     @param role 
     @param deadline 
     @param countdown 
     
     return type: Empty
     */
    public func usersUseridCaregroupsCaregroupidMembersMemberidRolesPost(memberid: String, userid: String, caregroupid: String, role: String, deadline: String?, countdown: String?) -> AWSTask<Empty> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    var queryParameters:[String:Any] = [:]
	    queryParameters["deadline"] = deadline
	    queryParameters["role"] = role
	    queryParameters["countdown"] = countdown
	    
	    var pathParameters:[String:Any] = [:]
	    pathParameters["memberid"] = memberid
	    pathParameters["userid"] = userid
	    pathParameters["caregroupid"] = caregroupid
	    
	    return self.invokeHTTPRequest("POST", urlString: "/users/{userid}/caregroups/{caregroupid}/members/{memberid}/roles", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: Empty.self) as! AWSTask<Empty>
	}




}
