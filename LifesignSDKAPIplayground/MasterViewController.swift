//
//  MasterViewController.swift
//  LifesignSDKAPIplayground
//
//  Created by Bo Dalberg on 28/04/2019.
//  Copyright © 2019 Bo Dalberg. All rights reserved.
//

import UIKit
import AWSCore
import AWSAPIGateway
import AWSMobileClient


class MasterViewController: UITableViewController {

    var detailViewController: DetailViewController? = nil
    var objects = [Any]()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        // Get the identity Id from the AWSIdentityManager
/*        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let credentialsProvider = AWSMobileClient.sharedInstance().getCredentialsProvider()
        let identityId = AWSIdentityManager.default().identityId*/
        
     /*   let credentialsProvider = AWSCognitoCredentialsProvider(regionType: AWSRegionType.EUCentral1, identityPoolId: "my_pool_id")
        let configuration = AWSServiceConfiguration(region: AWSRegionType.EUCentral1, credentialsProvider: credentialsProvider)
        AWSServiceManager.default().defaultServiceConfiguration = configuration*/
            
        
        let client = AWSLIFESIGNSDKLifesignClient.default()
        
//        client.usersPost(email: "georg@andeby.dk", name: "Georg Gearløs", cellno: "88776655", deviceid: "12121212", platform: "android").continueWith{ (task: AWSTask?) -> AnyObject? in
//        client.usersSearchGet(email: "anders@andeby.dk").continueWith{ (task: AWSTask?) -> AnyObject? in
        client.usersUseridGet(userid: "U-860a9650-69a2-11e9-b62a-05091a6fa6f9").continueWith { (task: AWSTask?) -> AnyObject? in
            if let error = task?.error {
                print("Error occurred: \(error)")
                return nil
            }
            else
            if let result = task?.result {

                // Do something with result
 //               var x = task!.result as! Empty
  
                print("The raw result is... \(result)")
                print("Task... \(String(describing: task)) ")
            }
            
            return nil
        }
        
        
    
        
        navigationItem.leftBarButtonItem = editButtonItem

        let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(insertNewObject(_:)))
        navigationItem.rightBarButtonItem = addButton
        if let split = splitViewController {
            let controllers = split.viewControllers
            detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }

    @objc
    func insertNewObject(_ sender: Any) {
        objects.insert(NSDate(), at: 0)
        let indexPath = IndexPath(row: 0, section: 0)
        tableView.insertRows(at: [indexPath], with: .automatic)
    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let object = objects[indexPath.row] as! NSDate
                let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
                controller.detailItem = object
                controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
        }
    }

    // MARK: - Table View

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objects.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

        let object = objects[indexPath.row] as! NSDate
        cell.textLabel!.text = object.description
        return cell
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            objects.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
        }
    }


}

